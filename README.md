# Terraform EBS Multi Attach

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| ami | A ID da imagem AMI a ser usada para as instâncias EC2. | `string` | `"ami-006dcf34c09e50022"` | no |
| device\_name | O nome do dispositivo usado para o volume EBS. | `string` | `"/dev/sdh"` | no |
| instance\_count | O número de instâncias EC2 a serem criadas. | `number` | `2` | no |
| instance\_type | O tipo de instância EC2 a ser usado. | `string` | `"t3.micro"` | no |
| multi\_attach\_enabled | Indica se o volume EBS pode ser anexado a várias instâncias EC2. | `bool` | `true` | no |
| region | A região da AWS onde os recursos serão criados. | `string` | `"us-east-1"` | no |
| security\_group\_ids | As IDs dos grupos de segurança da VPC. | `list(string)` | <pre>[<br>  "sg-0f60564c3b26811e6"<br>]</pre> | no |
| subnet\_id | A ID da subnet onde as instâncias EC2 serão criadas. | `string` | `"subnet-09c86875c0cc67743"` | no |
| volume\_iops | O número de IOPS provisionados para o volume EBS. | `number` | `100` | no |
| volume\_size | O tamanho do volume EBS em GiB. | `number` | `30` | no |
| volume\_size\_root | O tamanho do volume EBS em GiB para o / do sistema. | `number` | `8` | no |
| volume\_type | O tipo de volume EBS. | `string` | `"io1"` | no |
| volume\_type\_root | O tipo de volume EBS. | `string` | `"gp3"` | no |

## Outputs

No output.

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->