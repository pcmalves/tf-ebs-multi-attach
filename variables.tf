variable "region" {
  type        = string
  description = "A região da AWS onde os recursos serão criados."
  default     = "us-east-1"
}

variable "instance_count" {
  type        = number
  description = "O número de instâncias EC2 a serem criadas."
  default     = 2
}

variable "ami" {
  type        = string
  description = "A ID da imagem AMI a ser usada para as instâncias EC2."
  default     = "ami-006dcf34c09e50022"
}

variable "instance_type" {
  type        = string
  description = "O tipo de instância EC2 a ser usado."
  default     = "t3.micro"
}

variable "subnet_id" {
  type        = string
  description = "A ID da subnet onde as instâncias EC2 serão criadas."
  default     = "subnet-09c86875c0cc67743"
}

variable "security_group_ids" {
  type        = list(string)
  description = "As IDs dos grupos de segurança da VPC."
  default     = ["sg-0f60564c3b26811e6"]
}

variable "volume_size" {
  type        = number
  description = "O tamanho do volume EBS em GiB."
  default     = 30
}

variable "volume_size_root" {
  type        = number
  description = "O tamanho do volume EBS em GiB para o / do sistema."
  default     = 8
}

variable "volume_type_root" {
  type        = string
  description = "O tipo de volume EBS."
  default     = "gp3"
}

variable "volume_type" {
  type        = string
  description = "O tipo de volume EBS."
  default     = "io1"
}

variable "volume_iops" {
  type        = number
  description = "O número de IOPS provisionados para o volume EBS."
  default     = 100
}

variable "multi_attach_enabled" {
  type        = bool
  description = "Indica se o volume EBS pode ser anexado a várias instâncias EC2."
  default     = true
}

variable "device_name" {
  type        = string
  description = "O nome do dispositivo usado para o volume EBS."
  default     = "/dev/sdh"
}
