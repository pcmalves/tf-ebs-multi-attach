resource "aws_instance" "example" {
  count = var.instance_count

  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.security_group_ids

  root_block_device {
    volume_size = var.volume_size_root
    volume_type = var.volume_type_root
  }
}

resource "aws_ebs_volume" "example" {
  availability_zone    = aws_instance.example[0].availability_zone
  size                 = var.volume_size
  type                 = var.volume_type
  iops                 = var.volume_iops
  multi_attach_enabled = var.multi_attach_enabled
}

resource "aws_volume_attachment" "example" {
  count = length(aws_instance.example)

  device_name = var.device_name
  volume_id   = aws_ebs_volume.example.id
  instance_id = aws_instance.example[count.index].id
}
